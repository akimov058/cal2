package com.example.cal2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MyActivity extends Activity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */
    public EditText num_1;
    public EditText num_2;
    public Button add;
    public Button sub;
    public Button mult;
    public Button div;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        num_1=(EditText)findViewById(R.id.num_1);
        num_2=(EditText)findViewById(R.id.num_2);
        add = (Button) findViewById(R.id.add);
       sub = (Button) findViewById(R.id.sub);
        mult = (Button) findViewById(R.id.mult);
        div = (Button) findViewById(R.id.div);
       add.setOnClickListener(this);
        sub.setOnClickListener(this);
        mult.setOnClickListener(this);
        div.setOnClickListener(this);
    }
    // проверка заполнения
    private boolean isValidate(){
        boolean flag=true;
        if(num_1.getText().length()==0){
            num_1.setError(getString(R.string.error1));
            flag=false;
        }
        if(num_2.getText().length()==0){
            num_2.setError(getString(R.string.error1));
            flag=false;
        }
        return flag;

    }

    @Override
    public void onClick(View v) {
        if(!isValidate()) {
            return;
        }
        float num1;
        num1 = 0;
        float num2 = 0;
        float result = 0;
        String opera="";
        // читаем EditText и заполняем переменные числами
        num1 = Float.parseFloat(num_1.getText().toString());
        num2 = Float.parseFloat(num_2.getText().toString());

        // определяем нажатую кнопку и выполняем соответствующую операцию
        // в oper пишем операцию, потом будем использовать в выводе
        switch (v.getId()) {
            case R.id.add:
                opera = "+";
                result = num1 + num2;
                break;
            case R.id.sub:
                opera = "-";
                result = num1 - num2;
                break;
            case R.id.mult:
                opera = "*";
                result = num1 * num2;
                break;
            case R.id.div:
                opera = "/";
                result = num1 / num2;
                break;
            default:
                break;
        }

        // формируем строку вывода
        String resultstr;
        if(num2<0){
            resultstr = num1 + " " + opera + " (" + num2 + ") = " + result;
        } else {
            resultstr = num1 + " " + opera + " " + num2 + " = " + result;
        }
        // запуцскаем вторую активность и передаем результат
        Intent intent = new Intent(this, TwoActivity.class);
        intent.putExtra("resultstr", resultstr);
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}

